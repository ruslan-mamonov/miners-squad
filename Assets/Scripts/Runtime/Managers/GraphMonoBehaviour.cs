using Runtime.Configuration;
using Runtime.Player;
using Runtime.Utils;
using UnityEngine;

namespace Runtime.Managers
{
    public class GraphMonoBehaviour : MonoBehaviour
    {
        [SerializeField] private GameConfiguration gameConfiguration;

        private void Awake()
        {
            var ultimateJoystick = GameObject.Find("Minimalist Joystick 04").GetComponent<UltimateJoystick>();
            var screensGameObject = GameObject.Find("Screens");

            IScreensManagers screensManagers = new ScreensManager(screensGameObject);
            ILevelManager levelManager = new LevelManager(screensManagers);
            IPlayerMinionsManager playerMinionsManager = new PlayerMinionsManager();
            IGameDataWrapper gameDataWrapper = new GameDataWrapper();
            IPlayerView playerView = new PlayerView(gameConfiguration, this, levelManager, gameDataWrapper, ultimateJoystick);
            playerView.InitialPlayerComponents();
        }
    }
}
