using Runtime.Screens;
using UnityEngine;

namespace Runtime.Managers
{
    public interface IScreensManagers
    {
        void ShowWorkersShop();
    }

    public class ScreensManager : IScreensManagers
    {
        private readonly IWorkersShopScreenHandler _workersShopScreenHandler;

        public ScreensManager(GameObject screensGameObject)
        {
            var workersShopGameObject = screensGameObject.transform.Find("WorkersShop").gameObject;
            Debug.Log(workersShopGameObject);
            _workersShopScreenHandler = new WorkersShopScreenHandler(workersShopGameObject);
        }

        public void ShowWorkersShop()
        {
            _workersShopScreenHandler.ShowWorkersShop();
        }
    }
}
