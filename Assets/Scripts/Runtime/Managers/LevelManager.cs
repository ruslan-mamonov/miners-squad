using UnityEngine;

namespace Runtime.Managers
{
    public interface ILevelManager
    {
        void PlayerEnterTheMineZoneCallback(GameObject mainZoneGameObject);
        void PlayerExitTheMineZoneCallback(GameObject mainZoneGameObject);

        void PlayerEnterTheExchangeZoneCallback(GameObject exchangeZoneGameObject);
        void PlayerExitTheExchangeZoneCallback(GameObject exchangeZoneGameObject);

        void PlayerEnterTheWorkerShopZoneCallback(GameObject workerShopZoneGameObject);
        void PlayerExitTheWorkerShopZoneCallback(GameObject workerShopZoneGameObject);
    }

    public class LevelManager : ILevelManager
    {
        private readonly IScreensManagers _screensManagers;

        public LevelManager(IScreensManagers screensManagers)
        {
            _screensManagers = screensManagers;
        }

        public void PlayerEnterTheMineZoneCallback(GameObject mainZoneGameObject)
        {
        }

        public void PlayerExitTheMineZoneCallback(GameObject mainZoneGameObject)
        {
        }

        public void PlayerEnterTheExchangeZoneCallback(GameObject exchangeZoneGameObject)
        {
        }

        public void PlayerExitTheExchangeZoneCallback(GameObject exchangeZoneGameObject)
        {
        }

        public void PlayerEnterTheWorkerShopZoneCallback(GameObject workerShopZoneGameObject)
        {
            _screensManagers.ShowWorkersShop();
        }

        public void PlayerExitTheWorkerShopZoneCallback(GameObject workerShopZoneGameObject)
        {
        }
    }
}
