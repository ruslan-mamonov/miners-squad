using UnityEngine;

namespace Runtime.Managers
{
    public interface IPlayerMinionsManager
    {
        void AddNewAgent(Transform spawnPoint);
    }

    public class PlayerMinionsManager : IPlayerMinionsManager
    {
        public PlayerMinionsManager()
        {
        }

        public void AddNewAgent(Transform spawnPoint)
        {
        }
    }
}
