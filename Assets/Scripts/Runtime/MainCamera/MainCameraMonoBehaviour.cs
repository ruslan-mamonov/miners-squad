using System;
using UnityEngine;

namespace Runtime.MainCamera
{
    public class MainCameraMonoBehaviour : MonoBehaviour
    {
        private GameObject _playerGameObject;

        private void Start()
        {
            _playerGameObject = GameObject.Find("Player");
        }

        private void Update()
        {
            var playerPosition = _playerGameObject.transform.position;
            transform.position = new Vector3(playerPosition.x, transform.position.y, playerPosition.z + -5.25f);
        }
    }
}
