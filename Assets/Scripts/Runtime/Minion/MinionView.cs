namespace Runtime.Minion
{
    public interface IMinionView
    {
        void FollowToPlayer();
    }

    public class MinionView : IMinionView
    {
        public MinionView()
        {
        }

        public void FollowToPlayer()
        {
        }
    }
}
