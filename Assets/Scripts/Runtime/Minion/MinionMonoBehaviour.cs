using UnityEngine;
using UnityEngine.AI;

namespace Runtime.Minion
{
    public class MinionMonoBehaviour : MonoBehaviour
    {
        private NavMeshAgent _navMeshAgent;

        private void OnEnable()
        {
            _navMeshAgent = GetComponent<NavMeshAgent>();
        }

        private void Update()
        {
            _navMeshAgent.SetDestination(GameObject.Find("Player").transform.position);
        }
    }
}
