using UnityEngine;

namespace Runtime.Zones.Mine
{
    public class MineZoneMonoBehaviour : MonoBehaviour
    {
        [SerializeField] private MineResourceType mineMineResourceType;
        [SerializeField] private int resourcesAmount;

        public MineResourceType MineResourceType => mineMineResourceType;

        public int ResourcesAmount
        {
            get => resourcesAmount;
            set => resourcesAmount = value;
        }
    }

    public enum MineResourceType
    {
        Stone,
        Iron,
        Bronze,
        Silver,
        Gold,
        Diamond,
    }
}
