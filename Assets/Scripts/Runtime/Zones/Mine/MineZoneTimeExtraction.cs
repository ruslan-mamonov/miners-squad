using System;

namespace Runtime.Zones.Mine
{
    public class MineZoneTimeExtraction
    {
        public MineZoneTimeExtraction()
        {
        }

        public float GetTimeForResourceExtraction(MineResourceType mineResourceType, int mineZoneLevel)
        {
            return mineResourceType switch
            {
                MineResourceType.Stone => 1 * mineZoneLevel,
                MineResourceType.Iron => 2 * mineZoneLevel,
                MineResourceType.Bronze => 2 * mineZoneLevel,
                MineResourceType.Silver => 2 * mineZoneLevel,
                MineResourceType.Gold => 2 * mineZoneLevel,
                MineResourceType.Diamond => 2 * mineZoneLevel,
                _ => throw new ArgumentOutOfRangeException(nameof(mineResourceType), mineResourceType, null)
            };
        }
    }
}
