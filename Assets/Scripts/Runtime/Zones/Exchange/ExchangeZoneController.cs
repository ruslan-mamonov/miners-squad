using Runtime.Player.PlayerComponents;
using Runtime.Utils;
using UnityEngine;

namespace Runtime.Zones.Exchange
{
    public interface IExchangeZoneController
    {
        void ShowExchangeScreen();
        void HideExchangeScreen();
        void ExchangeResources();
    }

    public class ExchangeZoneController : IExchangeZoneController
    {
        private readonly IGameDataWrapper _gameDataWrapper;
        private readonly IPlayerInventoryController _playerInventoryController;

        public ExchangeZoneController(IGameDataWrapper gameDataWrapper, IPlayerInventoryController playerInventoryController)
        {
            _gameDataWrapper = gameDataWrapper;
            _playerInventoryController = playerInventoryController;
        }

        public void ShowExchangeScreen()
        {
        }

        public void HideExchangeScreen()
        {
        }

        public void ExchangeResources()
        {
            var currentStones = _gameDataWrapper.InventoryStoneAmount;
            var currentAmountOfGold = _gameDataWrapper.AmountOfGold;
            var exchangeResult = currentStones / 10;
            currentStones -= exchangeResult * 10;
            // _gameDataWrapper.AmountOfGold += _gameDataWrapper.InventoryStoneAmount / 10;
            Debug.Log($"Exchanged Golds: {currentStones} {currentAmountOfGold} {exchangeResult}");
            _gameDataWrapper.InventoryStoneAmount = currentStones;
        }
    }
}
