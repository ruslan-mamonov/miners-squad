using UnityEngine;

namespace Runtime.Utils
{
    public interface IGameDataWrapper
    {
        int CompletedLevels { get; set; }
        int AmountOfGold { get; set; }
        int InventoryStoneAmount { get; set; }
    }

    public class GameDataWrapper : IGameDataWrapper
    {
        public int CompletedLevels
        {
            get => PlayerPrefs.GetInt("CompletedLevelsData");
            set => PlayerPrefs.SetInt("CompletedLevelsData", value);
        }

        public int AmountOfGold
        {
            get => PlayerPrefs.GetInt("AmountOfGoldData");
            set => PlayerPrefs.SetInt("AmountOfGoldData", value);
        }

        public int InventoryStoneAmount
        {
            get => PlayerPrefs.GetInt("InventoryStoneAmountData");
            set => PlayerPrefs.SetInt("InventoryStoneAmountData", value);
        }
    }
}
