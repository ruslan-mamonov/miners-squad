using UnityEngine;
using UnityEngine.UI;

namespace Runtime.Screens
{
    public interface IWorkersShopScreenHandler
    {
        void ShowWorkersShop();
        void HideWorkersShop();
        void InitialListeners();
    }

    public class WorkersShopScreenHandler : IWorkersShopScreenHandler
    {
        private readonly GameObject _workersShopScreenGameObject;

        private Button _buyButton;
        private Button _exitButton;

        public WorkersShopScreenHandler(GameObject workersShopScreenGameObject)
        {
            _workersShopScreenGameObject = workersShopScreenGameObject;
        }

        public void ShowWorkersShop()
        {
            _workersShopScreenGameObject.SetActive(true);
        }

        public void HideWorkersShop()
        {
            _workersShopScreenGameObject.SetActive(false);
        }

        public void InitialListeners()
        {
            _exitButton.onClick.AddListener(HideWorkersShop);
        }
    }
}
