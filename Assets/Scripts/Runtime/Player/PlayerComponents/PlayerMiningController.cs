using System;
using System.Collections;
using Runtime.Zones.Mine;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Runtime.Player.PlayerComponents
{
    public interface IPlayerMiningController
    {
        Action<MineResourceType> OnReceivedResource { get; set; }
        void ExtractResource();
        void BeginMining(GameObject mineZoneTriggerGameObject);
        void StopMining();
    }

    public class PlayerMiningController : IPlayerMiningController
    {
        private readonly MonoBehaviour _monoBehaviour;
        private readonly IPlayerAnimatorController _playerAnimatorController;
        private readonly IPlayerInventoryController _playerInventoryController;

        private GameObject _mineZoneRootGameObject;
        private GameObject _mineZoneTriggerGameObject;
        private MineZoneMonoBehaviour _mineZoneMonoBehaviour;
        private IEnumerator _beginMiningEnumerator;

        public PlayerMiningController(MonoBehaviour monoBehaviour,
            IPlayerAnimatorController playerAnimatorController,
            IPlayerInventoryController playerInventoryController)
        {
            _monoBehaviour = monoBehaviour;
            _playerAnimatorController = playerAnimatorController;
            _playerInventoryController = playerInventoryController;
        }

        public Action<MineResourceType> OnReceivedResource { get; set; }

        public void ExtractResource()
        {
            if (_mineZoneMonoBehaviour.ResourcesAmount <= 0)
            {
                StopMining();
                return;
            }

            _mineZoneMonoBehaviour.ResourcesAmount -= 1;
            _playerInventoryController.AddResource(_mineZoneMonoBehaviour.MineResourceType, 1);
        }

        public void BeginMining(GameObject mineZoneTriggerGameObject)
        {
            _mineZoneRootGameObject = mineZoneTriggerGameObject.transform.root.gameObject;
            _mineZoneTriggerGameObject = mineZoneTriggerGameObject;
            _mineZoneMonoBehaviour = _mineZoneRootGameObject.GetComponent<MineZoneMonoBehaviour>();

            if (_mineZoneMonoBehaviour.ResourcesAmount <= 0) StopMining();

            _playerAnimatorController.PlayerMiningState(true);
        }

        public void StopMining()
        {
            // _monoBehaviour.StopCoroutine(_beginMiningEnumerator);
            _playerAnimatorController.PlayerMiningState(false);
        }

        private void FinishMining()
        {
            OnReceivedResource?.Invoke(_mineZoneMonoBehaviour.MineResourceType);
            _monoBehaviour.StopCoroutine(_beginMiningEnumerator);
            Object.Destroy(_mineZoneTriggerGameObject);
            _playerAnimatorController.PlayerMiningState(false);
        }
    }
}
