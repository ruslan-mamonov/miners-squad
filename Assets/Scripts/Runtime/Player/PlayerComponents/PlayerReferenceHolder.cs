using UnityEngine;

namespace Runtime.Player.PlayerComponents
{
    public class PlayerReferenceHolder : MonoBehaviour
    {
        [SerializeField] private GameObject playerModelGameObject;
        [SerializeField] private GameObject playerMeshGameObject;
        [SerializeField] private GameObject playerAnimatorGameObject;
        [SerializeField] private PlayerAnimationsCallbacks playerAnimationsCallbacks;

        public GameObject PlayerModelGameObject => playerModelGameObject;

        public GameObject PlayerMeshGameObject => playerMeshGameObject;

        public GameObject PlayerAnimatorGameObject => playerAnimatorGameObject;

        public PlayerAnimationsCallbacks AnimationsCallbacks => playerAnimationsCallbacks;
    }
}
