using System.Collections;
using UnityEngine;

namespace Runtime.Player.PlayerComponents
{
    public interface IPlayerMovement
    {
        void RemoveJoystickListeners();
    }

    public class PlayerMovement : IPlayerMovement
    {
        private const float PlayerMaxSpeed = 0.05f;

        private readonly MonoBehaviour _monoBehaviour;
        private readonly UltimateJoystick _ultimateJoystick;
        private readonly GameObject _playerGameObject;
        private readonly IPlayerAnimatorController _playerAnimatorController;

        private bool _playerMovementIsAvailable;
        private IEnumerator _updatePlayerTransformEnumerator;

        public PlayerMovement(MonoBehaviour monoBehaviour,
            UltimateJoystick ultimateJoystick,
            GameObject playerGameObject,
            IPlayerAnimatorController playerAnimatorController)
        {
            _monoBehaviour = monoBehaviour;
            _ultimateJoystick = ultimateJoystick;
            _playerGameObject = playerGameObject;
            _playerAnimatorController = playerAnimatorController;

            _ultimateJoystick.OnPointerDownCallback += OnPointerJoystickDown;
            _ultimateJoystick.OnPointerUpCallback += OnPointerJoystickUp;
        }

        public void RemoveJoystickListeners()
        {
            _ultimateJoystick.OnPointerDownCallback -= OnPointerJoystickDown;
            _ultimateJoystick.OnPointerUpCallback -= OnPointerJoystickUp;
        }

        private void OnPointerJoystickDown()
        {
            _playerMovementIsAvailable = true;

            _updatePlayerTransformEnumerator = UpdatePlayerTransformEnumerator();
            _monoBehaviour.StartCoroutine(_updatePlayerTransformEnumerator);
        }

        private void OnPointerJoystickUp()
        {
            _playerMovementIsAvailable = false;

            _monoBehaviour.StopCoroutine(_updatePlayerTransformEnumerator);
        }

        private IEnumerator UpdatePlayerTransformEnumerator()
        {
            while (_playerMovementIsAvailable)
            {
                var verticalAxis = _ultimateJoystick.GetVerticalAxis();
                var horizontalAxis = _ultimateJoystick.GetHorizontalAxis();
                var actualPlayerSpeed = _ultimateJoystick.GetDistance() * PlayerMaxSpeed;
                var transformEulerAngles = new Vector3(0, Mathf.Atan2(horizontalAxis, verticalAxis) * 180 / Mathf.PI, 0);

                _playerGameObject.transform.eulerAngles = transformEulerAngles;
                _playerGameObject.transform.Translate(_playerGameObject.transform.TransformDirection(Vector3.forward) * actualPlayerSpeed, Space.World);
                _playerAnimatorController.UpdatePlayerMovementSpeed(_ultimateJoystick.GetDistance());

                yield return null;
            }
        }
    }
}
