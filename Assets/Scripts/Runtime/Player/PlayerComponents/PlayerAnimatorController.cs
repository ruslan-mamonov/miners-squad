using UnityEngine;

namespace Runtime.Player.PlayerComponents
{
    public interface IPlayerAnimatorController
    {
        void PlayerMiningState(bool state);
        public void UpdatePlayerMovementSpeed(float speed);
    }

    public class PlayerAnimatorController : IPlayerAnimatorController
    {
        private static readonly int PlayerMiningBool = Animator.StringToHash("PlayerMiningBool");
        private static readonly int PlayerMovementSpeed = Animator.StringToHash("PlayerMovementSpeed");

        private readonly Animator _animator;

        public PlayerAnimatorController(Animator animator)
        {
            _animator = animator;
        }

        public void PlayerMiningState(bool state)
        {
            _animator.SetBool(PlayerMiningBool, state);
        }

        public void UpdatePlayerMovementSpeed(float speed)
        {
            _animator.SetFloat(PlayerMovementSpeed, speed);
        }
    }
}
