using System;
using Runtime.Utils;
using Runtime.Zones.Mine;
using UnityEngine;

namespace Runtime.Player.PlayerComponents
{
    public interface IPlayerInventoryController
    {
        void ShowInventory();
        void HideInventory();
        void AddResource(MineResourceType mineResourceType, int value);
    }

    public class PlayerInventoryController : IPlayerInventoryController
    {
        private readonly IGameDataWrapper _gameDataWrapper;

        public PlayerInventoryController(IGameDataWrapper gameDataWrapper)
        {
            _gameDataWrapper = gameDataWrapper;
        }

        public void ShowInventory()
        {
        }

        public void HideInventory()
        {
        }

        public void AddResource(MineResourceType mineResourceType, int value)
        {
            switch (mineResourceType)
            {
                case MineResourceType.Stone:
                    _gameDataWrapper.InventoryStoneAmount += value;
                    Debug.Log(_gameDataWrapper.InventoryStoneAmount);
                    break;
                case MineResourceType.Iron:
                    break;
                case MineResourceType.Bronze:
                    break;
                case MineResourceType.Silver:
                    break;
                case MineResourceType.Gold:
                    break;
                case MineResourceType.Diamond:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(mineResourceType), mineResourceType, null);
            }
        }
    }
}
