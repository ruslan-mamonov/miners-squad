using System;
using UnityEngine;

namespace Runtime.Player.PlayerComponents
{
    public interface IPlayerTrigger
    {
        Action<GameObject> OnPlayerEnterTheMineZone { get; set; }
        Action<GameObject> OnPlayerExitTheMineZone { get; set; }

        Action<GameObject> OnPlayerEnterTheExchangeZone { get; set; }
        Action<GameObject> OnPlayerExitTheExchangeZone { get; set; }

        Action<GameObject> OnPlayerEnterTheWorkersShopZone { get; set; }
        Action<GameObject> OmPlayerExitTheWorkersShopZone { get; set; }
    }

    public class PlayerTrigger : MonoBehaviour, IPlayerTrigger
    {
        private void OnTriggerEnter(Collider other)
        {
            switch (other.name)
            {
                case "MineZoneTrigger":
                    OnPlayerEnterTheMineZone?.Invoke(other.gameObject);
                    break;
                case "ExchangeTrigger":
                    OnPlayerEnterTheExchangeZone?.Invoke(other.gameObject);
                    break;
                case "ShopZoneTrigger":
                    OnPlayerEnterTheWorkersShopZone?.Invoke(other.gameObject);
                    break;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            switch (other.name)
            {
                case "MineZoneTrigger":
                    OnPlayerExitTheMineZone?.Invoke(other.gameObject);
                    break;
                case "ExchangeTrigger":
                    OnPlayerExitTheExchangeZone?.Invoke(other.gameObject);
                    break;
                case "ShopZoneTrigger":
                    OmPlayerExitTheWorkersShopZone?.Invoke(other.gameObject);
                    break;
            }
        }

        public Action<GameObject> OnPlayerEnterTheMineZone { get; set; }
        public Action<GameObject> OnPlayerExitTheMineZone { get; set; }
        public Action<GameObject> OnPlayerEnterTheExchangeZone { get; set; }
        public Action<GameObject> OnPlayerExitTheExchangeZone { get; set; }
        public Action<GameObject> OnPlayerEnterTheWorkersShopZone { get; set; }
        public Action<GameObject> OmPlayerExitTheWorkersShopZone { get; set; }
    }
}
