using System;
using UnityEngine;

namespace Runtime.Player.PlayerComponents
{
    public interface IPlayerAnimationsCallbacks
    {
        Action OnPlayerWorkHitAction { get; set; }
    }

    public class PlayerAnimationsCallbacks : MonoBehaviour, IPlayerAnimationsCallbacks
    {
        public void PlayerWorkHit()
        {
            OnPlayerWorkHitAction?.Invoke();
        }

        public Action OnPlayerWorkHitAction { get; set; }
    }
}
