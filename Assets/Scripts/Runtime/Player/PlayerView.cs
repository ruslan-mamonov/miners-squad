using Runtime.Configuration;
using Runtime.Managers;
using Runtime.Player.PlayerComponents;
using Runtime.Utils;
using Runtime.Zones.Exchange;
using UnityEngine;

namespace Runtime.Player
{
    public interface IPlayerView
    {
        void InitialPlayerComponents();
    }

    public class PlayerView : IPlayerView
    {
        private readonly GameConfiguration _gameConfiguration;
        private readonly MonoBehaviour _monoBehaviour;
        private readonly ILevelManager _levelManager;
        private readonly IGameDataWrapper _gameDataWrapper;
        private readonly UltimateJoystick _ultimateJoystick;

        private GameObject _playerGameObject;
        private Animator _playerAnimatorComponent;
        private PlayerReferenceHolder _playerReferenceHolder;
        private IPlayerAnimatorController _playerAnimatorController;
        private IPlayerAnimationsCallbacks _playerAnimationsCallbacks;
        private IPlayerMovement _playerMovement;
        private IPlayerMiningController _playerMiningController;
        private IPlayerInventoryController _playerInventoryController;
        private IPlayerTrigger _playerTrigger;

        private IExchangeZoneController _exchangeZoneController;

        public PlayerView(GameConfiguration gameConfiguration,
            MonoBehaviour monoBehaviour,
            ILevelManager levelManager,
            IGameDataWrapper gameDataWrapper,
            UltimateJoystick ultimateJoystick)
        {
            _gameConfiguration = gameConfiguration;
            _monoBehaviour = monoBehaviour;
            _levelManager = levelManager;
            _gameDataWrapper = gameDataWrapper;
            _ultimateJoystick = ultimateJoystick;
        }

        public void InitialPlayerComponents()
        {
            _playerGameObject = Object.Instantiate(_gameConfiguration.GameplayPrefabs.PlayerPrefab);
            _playerGameObject.name = "Player";
            _playerReferenceHolder = _playerGameObject.GetComponent<PlayerReferenceHolder>();
            _playerAnimatorComponent = _playerReferenceHolder.PlayerAnimatorGameObject.GetComponent<Animator>();
            _playerAnimatorController = new PlayerAnimatorController(_playerAnimatorComponent);
            _playerAnimationsCallbacks = _playerReferenceHolder.AnimationsCallbacks;
            _playerMovement = new PlayerMovement(_monoBehaviour, _ultimateJoystick, _playerGameObject, _playerAnimatorController);
            _playerInventoryController = new PlayerInventoryController(_gameDataWrapper);
            _playerMiningController = new PlayerMiningController(_monoBehaviour, _playerAnimatorController, _playerInventoryController);

            _playerAnimationsCallbacks.OnPlayerWorkHitAction += PlayerWorkHitAnimationCallback;

            _playerTrigger = _playerReferenceHolder.PlayerModelGameObject.GetComponent<PlayerTrigger>();
            _playerTrigger.OnPlayerEnterTheMineZone += PlayerEnterTheMineZoneCallback;
            _playerTrigger.OnPlayerExitTheMineZone += PlayerExitTheMineZoneCallback;
            _playerTrigger.OnPlayerEnterTheExchangeZone += PlayerEnterTheExchangeZoneCallback;
            _playerTrigger.OnPlayerExitTheExchangeZone += PlayerExitTheExchangeZoneCallback;
            _playerTrigger.OnPlayerEnterTheWorkersShopZone += OnPlayerEnterTheWorkersShopZoneCallback;
            _playerTrigger.OmPlayerExitTheWorkersShopZone += OnPlayerExitTheWorkersShopZoneCallback;

            _exchangeZoneController = new ExchangeZoneController(_gameDataWrapper, _playerInventoryController);
        }

        private void PlayerEnterTheMineZoneCallback(GameObject mineZoneTrigger)
        {
            _playerMiningController.BeginMining(mineZoneTrigger.transform.root.gameObject);
        }

        private void PlayerExitTheMineZoneCallback(GameObject mineZoneTrigger)
        {
            _playerMiningController.StopMining();
        }

        private void PlayerEnterTheExchangeZoneCallback(GameObject exchangeZoneTrigger)
        {
            _exchangeZoneController.ShowExchangeScreen();

            _exchangeZoneController.ExchangeResources();
        }

        private void PlayerExitTheExchangeZoneCallback(GameObject exchangeZoneTrigger)
        {
            _exchangeZoneController.HideExchangeScreen();
        }

        private void PlayerWorkHitAnimationCallback()
        {
            _playerMiningController.ExtractResource();
        }

        private void OnPlayerEnterTheWorkersShopZoneCallback(GameObject workersShopZoneTrigger)
        {
            _levelManager.PlayerEnterTheWorkerShopZoneCallback(workersShopZoneTrigger.transform.root.gameObject);
        }

        private void OnPlayerExitTheWorkersShopZoneCallback(GameObject workersShopZoneTrigger)
        {
            _levelManager.PlayerExitTheWorkerShopZoneCallback(workersShopZoneTrigger.transform.root.gameObject);
        }
    }
}
