using System;
using UnityEngine;

namespace Runtime.Configuration
{
    [CreateAssetMenu(fileName = "GameConfiguration", menuName = "ScriptableObjects/Game Configuration", order = 1)]
    public class GameConfiguration : ScriptableObject
    {
        [SerializeField] private GameplayPrefabs gameplayPrefabs;

        public GameplayPrefabs GameplayPrefabs => gameplayPrefabs;
    }

    [Serializable]
    public class GameplayPrefabs
    {
        [SerializeField] private GameObject playerPrefab;

        public GameObject PlayerPrefab => playerPrefab;
    }
}
