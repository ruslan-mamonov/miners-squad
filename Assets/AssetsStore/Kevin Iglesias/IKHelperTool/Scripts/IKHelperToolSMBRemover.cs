﻿///////////////////////////////////////////////////////////////////////////
//  IK Helper Tool 1.1 - IK Remover StateMachineBehaviour                //
//  Kevin Iglesias - https://www.keviniglesias.com/     			     //
//  Contact Support: support@keviniglesias.com                           //
//  Documentation: 														 //
//  https://www.keviniglesias.com/assets/IKHelperTool/Documentation.pdf  //
///////////////////////////////////////////////////////////////////////////

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//For custom inspector only
#if UNITY_EDITOR
	using UnityEditor;
#endif

namespace KevinIglesias {

	public class IKHelperToolSMBRemover : StateMachineBehaviour
	{
		private IKHelperTool iKHTScript;
		
		public int id;
		
		public bool clearAll;
		
		public bool smoothExit;
		
        public float delay;
        
		public float speed;
		
		// OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
		override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			if(iKHTScript == null)
			{
				iKHTScript = animator.GetComponent<IKHelperTool>();
               
			}
			
			if(iKHTScript != null)
			{
				iKHTScript.ClearIK(clearAll, id, delay, smoothExit, speed);
			}
		}
	}
    
#if UNITY_EDITOR
	//Custom Inspector
	[CustomEditor(typeof(IKHelperToolSMBRemover))]
    public class IKHelperToolSMBRemoverCustomInspector : Editor
    {

		public override void OnInspectorGUI()
		{
			var smbScript = target as IKHelperToolSMBRemover;

			
			GUI.enabled = true;
            GUILayout.Space(5);
			
			if(smbScript is {clearAll: false})
			{
				GUILayout.BeginHorizontal();
				EditorGUI.BeginChangeCheck();
				EditorStyles.label.fontStyle = FontStyle.Bold;
				int iID = EditorGUILayout.IntField("State IK ID:", smbScript.id);
				EditorStyles.label.fontStyle = FontStyle.Normal;
				if(EditorGUI.EndChangeCheck()) {
#pragma warning disable 618
					Undo.RegisterUndo(target, "Changed ID");
#pragma warning restore 618
					smbScript.id = iID;
				}
				GUILayout.EndHorizontal();
			}
			
			GUILayout.BeginHorizontal();
			EditorGUI.BeginChangeCheck();
			bool iBool = EditorGUILayout.Toggle("Clear All IK States", smbScript.clearAll);
			if(EditorGUI.EndChangeCheck()) {
#pragma warning disable 618
				Undo.RegisterUndo(target, "Clear All IK States");
#pragma warning restore 618
				smbScript.clearAll = iBool;
			}
			GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
			EditorGUI.BeginChangeCheck();
			float iFloat = EditorGUILayout.FloatField("Delay (seconds)", smbScript.delay);
			if(EditorGUI.EndChangeCheck()) {
#pragma warning disable 618
				Undo.RegisterUndo(target, "Change Delay");
#pragma warning restore 618
				smbScript.delay = iFloat;
			}
			GUILayout.EndHorizontal();
            
			GUILayout.BeginHorizontal();
			EditorGUI.BeginChangeCheck();
			iBool = EditorGUILayout.Toggle("Smooth Exit", smbScript.smoothExit);
			if(EditorGUI.EndChangeCheck()) {
#pragma warning disable 618
				Undo.RegisterUndo(target, "Change Smooth Exit");
#pragma warning restore 618
				smbScript.smoothExit = iBool;
			}
			GUILayout.EndHorizontal();
			
			if(smbScript.smoothExit)
			{
				GUI.enabled = true;
			}else{
				GUI.enabled = false;
			}
			
            if(smbScript.speed < 0.01f)
            {
                smbScript.speed = 0.01f;
            }
            
			GUILayout.BeginHorizontal();
			EditorGUI.BeginChangeCheck();
			iFloat = EditorGUILayout.FloatField("Speed (seconds):", smbScript.speed);
			if(EditorGUI.EndChangeCheck()) {
#pragma warning disable 618
				Undo.RegisterUndo(target, "Change IK Speed");
#pragma warning restore 618
				smbScript.speed = iFloat;
			}
			GUILayout.EndHorizontal();
			GUI.enabled = true;
			
			GUILayout.Space(2);
			
			GUILayout.BeginHorizontal();
			GUIContent buttonContent;
			if(EditorApplication.isPlaying)
			{
				if(IKHelperUtils.clearClipboard)
				{
					buttonContent = new GUIContent("Variables copied!", "Copy changes made in Play Mode");
				}else{
					buttonContent = new GUIContent("Copy variables", "Copy changes made in Play Mode");
				}
				if(GUILayout.Button(buttonContent))
				{
					IKHelperUtils.savedRemoverSMB.id = smbScript.id;
					IKHelperUtils.savedRemoverSMB.clearAll = smbScript.clearAll;
					IKHelperUtils.savedRemoverSMB.smoothExit = smbScript.smoothExit;
					IKHelperUtils.savedRemoverSMB.delay = smbScript.delay;
					IKHelperUtils.savedRemoverSMB.speed = smbScript.speed;
					
					IKHelperUtils.clearClipboard = true;
				}
			}else{
				
				GUI.enabled = IKHelperUtils.clearClipboard;
				buttonContent = new GUIContent("Paste variables", "Paste changes made in Play Mode");
				if(GUILayout.Button(buttonContent))
				{
					EditorGUI.BeginChangeCheck();
					GUI.changed = true;
					
					if(EditorGUI.EndChangeCheck()) {
#pragma warning disable 618
						Undo.RegisterUndo(target, "Pasted variables");
#pragma warning restore 618
					}
					
					smbScript.id = IKHelperUtils.savedRemoverSMB.id;
					smbScript.clearAll = IKHelperUtils.savedRemoverSMB.clearAll;
					smbScript.smoothExit = IKHelperUtils.savedRemoverSMB.smoothExit;
					smbScript.delay = IKHelperUtils.savedRemoverSMB.delay;
					smbScript.speed = IKHelperUtils.savedRemoverSMB.speed;
                    
                    Debug.Log("Pasted variables.");
				}	
				GUI.enabled = true;
			}
			GUILayout.EndHorizontal();
			
			if(GUI.changed)
            {
                if(!EditorApplication.isPlaying)
				{
					EditorUtility.SetDirty(target);
#pragma warning disable 618
					EditorApplication.MarkSceneDirty();
#pragma warning restore 618
				}
            }
		}
	}
#endif	
}
